#lang play
(require redex)
(require "sta.rkt")
(require "sta-semantics.rkt")

;TYPE-TEST

;Principal can improve (primitive) type information via mapping.
;CASE
(define mapping1 
    (make-hash 
     '(
       ("h" . 
          #hash(((AT "fh") . INT) ((AT "celsius") . (AT "fh"))))
     ("root" . #hash(((AT "fh") . INT))))))
(define inter1 (new (Interpreter mapping1))) 
(test-equal 
 (send inter1 type-check (term X) '([X : (AT "fh")]))
 '(INT))

;CASE
;;Principal can improve (function) type information via mapping. 
(define mapping2
    (make-hash 
     '(("root" . #hash(((AT "fh") . INT))))))

(define inter2 (new (Interpreter mapping2))) 
(test-equal 
 (send inter2 type-check (term X) '([X : (→ (AT "fh") (AT "fh"))]))
 '((→ INT INT)))

;CASE
;Function most have the most concrete view of the argument type
;Doesn't type
(test-equal 
 (send inter2 type-check
             (term (λ X : (AT "fh") 1)) '
             ([X : (→ (AT "fh") (AT "fh"))]))
 '())
;it types
(test-equal 
 (send inter2 type-check
             (term (λ X : (AT "ABC") 1)) '
             ([X : (→ (AT "fh") (AT "fh"))]))
 '((→ (AT "ABC") INT)))


;CASE
;Principal must know the abstract type to export values
(define mapping3
    (make-hash 
     '(("h" . #hash(((AT "fh") . INT))))))
(define inter3 (new (Interpreter mapping3))) 
(test-equal
 (send inter3 type-check 
  (term (let "h" f (→ (AT "fh") (AT "fh")) (λ X : INT X) in f)) 
  '())
 '((→ (AT "fh") (AT "fh"))))

;CASE
;Embedding
(test-equal
 (send inter3 type-check 
   (term (emb 3 ("h" "c") (AT "fh"))) 
   '())
 '((AT "fh")))


(define mapping4
    (make-hash 
     '(("h" . #hash(((AT "fh") . INT)))
       ("root" . #hash(((AT "fh") . INT))))))

(define inter4 (new (Interpreter mapping4)))
;CASE
(test-equal
 (send inter4 type-check 
   (term (emb 3 ("h" "c") (AT "fh"))) 
   '())
 '(INT))


;************************************************************************************
;REDUCTION-TEST

(define interEmpty (new (Interpreter (make-hash '()))))
;\begin{STLC TEST}
;CASE: 
(test-equal
 (send interEmpty reduce* (+ 1 2))
 3)


#|CASE
{Description: E-Abs, E-App, E-op2 works}
|#
(test-equal
 (send interEmpty reduce* (term ( ((λ X : (→ INT INT) X) (λ X : INT (+ X 1))) (+ 2 3))))
 6)

;\end{STLC TEST}

;\begin{STA TEST}
;CASE
(test-equal
 (send interEmpty reduce* (term (let "abc" X (AT "fh") 1 in X )))
 '(emb 1 ("abc") (AT "fh"))
 )
;CASE
(test-equal
 (send inter1 reduce* (term (let "h" X (AT "fh") 1 in X )))
 1) 

;CASE
(test-equal
 (send inter1 reduce* (term (let "h" X (AT "fh") (+ 1 2) in (+ X 5))))
 8)

;CASE
(test-equal
 (send inter1 reduce* (term (let "h" X (AT "fh") (+ 1 2) in (+ X 5))))
 8)

;CASE
(define mapping5 
    (make-hash 
     '(
       ("p1" . 
             #hash(((AT "t1") . (AT "t2"))))
       ("p2" . 
             #hash(((AT "t2") . (AT "t3"))))
       ("p3" . 
             #hash(((AT "t3") . INT)))
       ("root" . 
               #hash(((AT "t1") . (AT "t2"))
                     ((AT "t2") . (AT "t3"))
                     ((AT "t3") . INT))))))
(define inter5 (new (Interpreter mapping5)))
(test-equal (send inter5 reduce* (term (let "p1" x_1 (AT "t1")
                             (let "p2" x_2 (AT "t2")
                               (let "p3" x_3 (AT "t3")
                                 1
                                 in
                                 x_3)
                               in
                               x_2)
                             in
                             x_1)))
            1)

;ÇASE
(define mapping5_1 
    (make-hash 
     '(
       ("p1" . 
             #hash(((AT "t1") . (AT "t2"))))
       ("p2" . 
             #hash(((AT "t2") . (AT "t3"))))
       ("p3" . 
             #hash(((AT "t3") . INT)))
       ("root" . 
               #hash(((AT "t1") . (AT "t2"))
                     ((AT "t2") . (AT "t3")))))))

(define inter5_1 (new (Interpreter mapping5_1)))
(test-equal (send inter5_1 reduce* (term (let "p1" x_1 (AT "t1")
                             (let "p2" x_2 (AT "t2")
                               (let "p3" x_3 (AT "t3")
                                 1
                                 in
                                 x_3)
                               in
                               x_2)
                             in
                             x_1)))
            '(emb 1 ("p3" "p2" "p1") (AT "t3")))

;CASE
(define mapping5_2
    (make-hash 
     '(
       ("p1" . 
             #hash(((AT "t1") . (AT "t2"))))
       ("p2" . 
             #hash(((AT "t2") . (AT "t3"))))
       ("p3" . 
             #hash(((AT "t3") . INT)))
       )))
(define inter5_2 (new (Interpreter mapping5_2)))
(test-equal (send inter5_2 reduce* (term (let "p1" x_1 (AT "t1")
                             (let "p2" x_2 (AT "t2")
                               (let "p3" x_3 (AT "t3")
                                 1
                                 in
                                 x_3)
                               in
                               x_2)
                             in
                             x_1)))
            '(emb 1 ("p3" "p2" "p1") (AT "t1")))
;CASE TYPE-CHECK TEST
(test-equal (send inter5_2 type-check (term (emb 1 ("p3" "p2" "p1") (AT "t1"))))
            '((AT "t1")))
;CASE TYPE-CHECK TEST
(test-equal (send inter5_2 type-check (term (emb 1 ("p3" "p5" "p1") (AT "t1"))))
            '())

;CASE .
;description: This is the case of Fig 11 in the paper STA
(define mapping6
    (make-hash 
     '(
       ("root" . 
             #hash(((AT "fh") . INT)))
       )))
(define inter6 (new (Interpreter mapping6)))
(test-equal (send inter6 reduce* (term ((λ Y : (→ INT INT) (Y 3)) 
                            (emb (λ X : (AT "fh") X) ("c") (→ (AT "fh") (AT "fh"))))))
            3)
(test-equal (send inter6 type-check (term ((λ Y : (→ INT INT) (Y 3)) 
                            (emb (λ X : (AT "fh") X) ("c") (→ (AT "fh") (AT "fh"))))))
            '(INT))
;CASE
(define mapping6_1
    (make-hash 
     '(      
       )))
(define inter6_1 (new (Interpreter mapping6_1)))
(test-equal (send inter6_1 reduce* (term ((λ Y : (→ INT INT) (Y 3)) 
                            (emb (λ X : (AT "fh") X) ("c") (→ (AT "fh") (AT "fh"))))))
            "no reduce")
(test-equal (send inter6_1 type-check (term ((λ Y : (→ INT INT) (Y 3)) 
                            (emb (λ X : (AT "fh") X) ("c") (→ (AT "fh") (AT "fh"))))))
            '())

;\end{STA TEST}
;************************************************************************************