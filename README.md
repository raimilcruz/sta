# README #

This repository contains an implementation of the multi-agent calculus of Grossman et al.(http://www.cis.upenn.edu/~stevez/papers/GMZ00.pdf). The calculus that we have implemented here is slightly different to the one they presented. This implementation is in Redex.